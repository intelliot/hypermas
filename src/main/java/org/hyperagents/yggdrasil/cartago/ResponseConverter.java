package org.hyperagents.yggdrasil.cartago;

public interface ResponseConverter {

  Object convert(Object o);
}

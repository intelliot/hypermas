package org.hyperagents.yggdrasil.cartago;

public interface ArgumentConverter {

  Object[] convert(Object[] args);
}
